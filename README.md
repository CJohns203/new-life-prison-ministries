# New Life Prison Ministries

New Life Prison Ministries (NLPM) was built using WordPress, serving as their primary website, allowing prison chaplains who register through the site to order curriculum for their students.

Together our team scoped out the clients needs during an initial discovery phase, which was used to develop and implement an information architecture plan.

Once our designer and project manager had design approval from the client, my role was to build the site with WordPress using Beaver Builder and WooCommerce, as well as ensuring all technical requirements were met.

The checkout requirements for chaplains ordering curriculum through the site was built using custom WooCommerce layouts and functionality.

Beaver Builder is a highly effective tool for rapidly building reusable custom responsive layouts in WordPress.


[New Life Prison Minitries site](https://nlpm.ca)

[Beaver Builder Page Builder](https://www.wpbeaverbuilder.com)

[WooCommerce](https://woocommerce.com)

